#!/usr/bin/env python3
import xml.etree.ElementTree as ET
import os
import argparse
from string import Template

parser = argparse.ArgumentParser(description='Take Filenames')
parser.add_argument('file', type=open, nargs=1, help='synthv1 preset filename')
presetfilelocation = parser.parse_args()
# get xml preset file
fileloc = presetfilelocation.file[0]
tree = ET.parse(fileloc)
root = tree.getroot()

presetname = root.get("name")


# create the lv2 directory

lv2dirname = ("synthv1_" + presetname + ".preset.lv2")

if not os.path.exists(lv2dirname):
   os.makedirs(lv2dirname)

# generate the manifest file

manifestfile = open(lv2dirname + "/manifest.ttl", "w")
manifesttemplate = open( "manifest.template", "r" )
manifestprt = Template( manifesttemplate.read() )
manetemp={ 'presetname':presetname }
manisubstitute = manifestprt.substitute(manetemp)
manifestfile.write(manisubstitute)
manifestfile.close

# generate the preset file

presetfile = open(lv2dirname + "/" + presetname + ".ttl", "w")
presettemplate = open( "preset.template", "r" )
presetprt = Template (presettemplate.read())

valsout=list()

for elem in root:
   countelm = len(elem)
   for subelem in elem:
      lv2sym = subelem.get("name")
      lv2val = subelem.text
      valsout.append("\t\t" + 'lv2:symbol "' + lv2sym + '" ;')
      valsout.append("\t\t" + 'pset:value ' + lv2val)
      countelm -= 1
      if countelm!=0:
        valsout.append("\t] , [")

configvalues=( "\n".join(valsout) )

presettemp={ 'presetname':presetname , 'configvalues':configvalues }
presetsubstitute = presetprt.substitute(presettemp)
presetfile.write(presetsubstitute) 
presetfile.close
