Native Synthv1 presets available here <http://linuxsynths.com/Synthv1PatchesDemos/synthv1.html>


usage: synthv12lv2.py [-h] file

Take Filenames

positional arguments:
  file        synthv1 preset filename

optional arguments:
  -h, --help  show this help message and exit
